### **In the Name of Allah, the Most Beneficent, the Most Merciful**

*This is my exercise repo, where you can find my all Python exercise, that I have learnt from scratch*


1. Basics Review
    1. Introduction
    2. The Python Type Hierarchy
    3. Multi-line Statements and String
    4. Variable Names
    5. Conditionals
    6. Functions
    7. The While Loop
    8. Break, Continue and the Try
    9. The For Loop
    10. Classes

2. Variable and Memory
    1. Introduction
    2. Variables are Memory Reference
    3. Reference Counting
    4. Garbage Collection
    5. Dynamic vs Static
    6. Variable reassignment
    7. Object Mutability vs Immutability
    8. Function Argument mutability
    9. Shared references and mutability
    10. Everything is an object
    11. Variable Equality
    12. Python Optimizations: Interning
    13. Python Optimizations: String Interning
    14. Python Optimizations: Peephole

3. Numeric Types
    1. Introduction
    2. Integers: Data Types
    3. Integers: Operations
    4. Integers: Constructors and Bases (Theory)
    5. Integers: Constructors and Bases (Coding)
    6. Rational Number (Theory)
    7. Rationals Number (Coding)
    8. Floats: Internal Representations (Theory)
    9. Floats: Internal Representations (Coding)
    10. Floats: Equality Testing (Theory)
    11. Floats: Equality Testing (Coding)
    12. Floats: Coercing to Integers (Theory)
    13. Floats: Coercing to Integers (Coding)
    14. Floats: Rounding (Theory)
    15. Floats: Rounding (Coding)
    16. Decimals (Theory)
    17. Decimals (Coding)
    18. Decimals: Constructors and Contexts (Theory)
    19. Decimals: Constructors and Contexts (Coding)
    20. Decimals: Math Operations: (Theory)
    21. Decimals: Math Operations: (Coding)
    22. Decimals: Performance Considerations
    23. Complex Numbers: (Theory)
    24. Complex Numbers: (Coding)
    25. Booleans
    26. Booleans: Truth Values (Theory)
    27. Booleans: Truth Values (Coding)
    28. Booleans: Precedence and Short-Circuiting (Theory)
    29. Booleans: Precedence and Short-Circuiting (Coding)
    30. Booleans: Boolean Operators (Theory)
    31. Booleans: Boolean Operators (Coding)
    32. Comparison Operator
4. Function Parameters
5. First-Class Function
6. Scopes, Closures and Decorators
7. Tuples as Data Structures and Named Tuples
8. Modules, Packages and Namespaces
9. Extras
