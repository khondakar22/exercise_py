# Below example python manager create the shared reference
a = 10  # Python just has to point to the existing reference for 10
b = 10
# But below this one python doesn't create the shared reference

a = 500  # Python does not use that global list and a new object is created every time
b = 500

# Why?
# Interning : reusing objects on-demand
# At startup, Python(CPython), pre-loads (caches) a global list of integers in the range [-5, 256]
# Any time an integer is referenced in that range, Python will use the cached version of that object
# [-5, 256] are Singletons Object (Class  only instantiate once] - Optimization strategy - small integers show up often


a = 10
b = 10

print(id(a))  # It is safe because integer is immutable
print(id(b))

a = -5
b = -5
print(id(a), id(b))
print(a is b)

a = 257
b = 257
print(a is b)  # false

a = 10
b = int(10)
c = int('10')
d = ('1010', 2)

print(a, b, c, d)

print(id(a), id(b), id(c), id(d))