# The variable my_var is purely a reference to a string object with value hello.
# No type is "attached" to my_var

my_var = 'hello'

# Now you can change the my_var as integer

my_var = 10

# Remember: Variables in Python do not have an inherited static type

a = 'hello'
print(type(a))
a = 10
print(type(a))
a = lambda x: x**2
print(type(a))
a = 3**2j
print(type(a))
