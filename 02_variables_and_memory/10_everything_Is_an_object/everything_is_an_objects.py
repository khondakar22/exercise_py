# Any object can be assigned to a variable including functions
# Any object can be passed to a function including functions
# Any object can be returned from a function including functions

# my_func without parentheses is the name of the function
# my_func() invokes the function


a = 10
print(type(a))

b = int(10)
print(type(b))

# Class documentation

print(help(int))

c = int()
print(c)
c = int('101', base=2)
print(c)

# Functions are object two


def square(a):
    return a ** 2


print(type(square))

f = square

print(id(f))
print(id(square))

print(f is square)

# Invoking

print(square(2))
print(f(2))


def cub(a):
    return a ** 3


def select_function(fn_id):
    if fn_id == 1:
        return square
    else:
        return cub


f = select_function(1)

print(f is square)
print(f(2))

f = select_function(2)
print(f is cub)
print(f(2))

# without assigning any variable
print(select_function(2)(3))

# function passed


def exec_func(fn, n):
    return fn(n)


# Function is the first class citizen

print(exec_func(cub, 3))
print(exec_func(square, 2))
