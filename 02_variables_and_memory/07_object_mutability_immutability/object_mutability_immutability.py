# Changing the data inside the object is called modifying the internal state of the object
# But memory address does not changed

# An object whose internal state can be changed, is called Mutable
# An object whose internal state can not be changed, is called Mutable

# Example in Python

# Immutable
# --------------------------------- #
# Number (Int, float, Booleans, etc)
# Strings
# Tuples
# Frozen Sets
# User-Defined Classes


# Mutable
# --------------------------------- #
# Lists
# Sets
# Dictionaries
# User-Defined Classes


# Warning about Mutability and Immutability
# --------------------------------- #
# t, Tuples are immutable: elements cannot be deleted, inserted, or replaced
# In this case, both the container (tuple), and all its elements (ints) are immutable
t = (1, 2, 3)
# But consider this:
a = [1, 2]
b = [3, 4]
# List are mutable: elements can be deleted, inserted, or replaced
# so then what happened if we do like below
t = (a, b)  # t = ([1,2], [3,4])
# If we insert some element to List
a.append(3)
b.append(5)
# So
print(t)  # t = ([1,2,3], [3,4,5])
# Here tuple is definitely immutable, we are changing the list not the pointer of tuple memory address
# Some example

my_list = [1, 2, 3]
print(type(my_list))
print(hex(id(my_list)))

my_list.append(4)
print(my_list)
print(hex(id(my_list)))
# above example you can see the, the memory address of my_list is same after inserted value 4 by using append

# But, There is exception, If you concat the value in a list then the memory address will change, see below example
my_list_1 = [1, 2, 3]
print(hex(id(my_list_1)))

my_list_1 = my_list_1 + [4]
print((hex(id(my_list_1))))

# Using dictionary

my_dict = dict(key1=1, key2='a')
print(my_dict)
print(hex(id(my_dict)))

# Now insert some key value
my_dict['key3'] = 10.5
print(my_dict)
print(hex(id(my_dict)))  # The memory address won't change
