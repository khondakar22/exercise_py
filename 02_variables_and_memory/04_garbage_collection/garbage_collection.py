import ctypes
import gc


def ref_count(address):
    # This function return the memory address of objects
    return ctypes.c_long.from_address(address).value


def object_by_id(object_id):
    # This one dig into the garbage collector and tell us whether the object in garbage collector
    for obj in gc.get_objects():
        if id(obj) == object_id:
            return "Object exists"
    return "Not Found"


class A:
    # Create Class to find the circular reference

    # Constructor
    def __init__(self):
        self.b = B(self)
        print('A: self: {0}, b: {1}'.format(hex(id(self)), hex(id(self.b))))


class B:
    # Constructor
    def __init__(self, a):
        self.a = a
        print('B: self: {0}, a: {1}'.format(hex(id(self)), hex(id(self.a))))


# For checking the circular reference
gc.disable()

# Instantiate the Class A
my_var = A()

# Below print option can give you overview of circular reference
print(my_var)
print(hex(id(my_var)))
print(hex(id(my_var.b)))
print(hex(id(my_var.b.a)))

# Count the reference by calling ref_count()
a_id = id(my_var)
b_id = id(my_var.b)

print(ref_count(a_id))
print(ref_count(b_id))

# Looking object is exists or not

print(object_by_id(a_id))
print(object_by_id(b_id))

# Destroying the my_var object
my_var = None

# Now see again the ref_count()
print(ref_count(a_id))
print(ref_count(b_id))

# Now you can see the object is still exist or not
print(object_by_id(a_id))
print(object_by_id(b_id))

# Now we run the garbage collector manually
gc.collect()

# Now you can see the object is still exist or not
print(object_by_id(a_id))
print(object_by_id(b_id))




