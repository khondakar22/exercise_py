# In Python, String(str) are immutable objects.
# Once a string has been created, the contents of the
# Object can never be changed
# The only way to modify the "value" of my_var is to re-assign
# the variable to another object

# Immutable objects are safe from unintended side-effects
# Think about Scopes
# Module scope and function() scope
# Below example can illustrate that the process() function
# can re-assign but can't change the value of my_var from module Scope or global scope
# On the contrast, Mutable object are not safe from unintended side-effect
# Why? Because, sometime you need your global variable scope to use other function in module
# So if it changed by one function, then you can't use the same value of that global variable
# And it will give you other modified value. It's not a safe,
# It's pass by reference

# Example of Immutability


def process(s):
    print('Initial s # = {0}'.format(id(s)))
    # now Modified the s
    s = s + 'world'
    print('Final s # = {0}'.format(id(s)))

    my_var = 'hello'
    print('my_var # = {0}'.format(id(my_var)))

    process(my_var)

    # Immutable my_var in module scope
    print('my_var # = {0}'.format(id(my_var)))

# Example for List of Mutability


def modify_list(lst):
    print('Initial list # = {0}'.format(id(lst)))
    lst.append(100)
    print('Final list # = {0}'.format(id(lst)))


my_list = [1, 2, 3]
print(id(my_list))

# Now pass the reference of my_list to modify_list(lst) function
# We will see the same address of my_list after modifying the list
# And it will change the value, the value will be [1,2,3,100]
modify_list(my_list)
