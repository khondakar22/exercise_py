# how to compare two variable in two fundamental ways:
#    Memory Address            Object State (data)
#       is                          ==
#       identity operator           equality operator (which will compare the internal state of object
#       var_1 is var_2
#       Negation (is not)           !=
#       var_1 is not var_2          var_1 != var_2
#       not(var_1 is var_2)         not( var_1 == var_2)

# None Object -- can be assigned to variables to indicate that they are not set
# Always create shared reference


a = 10
b = 10

print(id(a))
print((id(b)))
print("a is b", a is b)
print("a == b", a == b)


# Not a shared reference
a = 500
b = 500
print(id(a))
print(id(b))

print("a is b", a is b)  # false
print("a == b", a == b)  # true

a = [1, 2, 3]
b = [1, 2, 3]
print("a is b", a is b)  # false
print("a == b", a == b)  # true

a = 10
b = 10.0

print("a is b", a is b)  # false
print("a == b", a == b)  # true

# lets see for complex number

a = 10 + 0j
b = 10
print(type(a))
print(type(b))

print("a is b", a is b)  # false
print("a == b", a == b)  # true

# None

a = None
b = None
c = None

print("a is b", a is b)  # true
print("a is c", a is c)  # true
print("a is None", a is None)  # true
print("c is None", c is None)  # true
print("b is None", b is None)  # true

