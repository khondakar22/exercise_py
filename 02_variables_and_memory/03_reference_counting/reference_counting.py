# Finding the reference count
# sys.getrefcount(my_var) <- passing my_var to getrefcount() creates an extra reference!
import sys
import ctypes
a = [1, 2, 3]
print(id(a))

print(sys.getrefcount(a))  # output will be 2

# By using the ctypes it won't create an extra reference


def ref_count(address: int):
    return ctypes.c_long.from_address(address).value


print(ref_count(id(a)))
