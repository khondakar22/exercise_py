import string
import time


# This is another variety of optimizations that can occur at compile time.
# Constant expressions
#               numeric calculation 24 * 60 python will actually pre-calculate 24*60 -> 1440
#               short sequences length < 20
#                           (1,2) * 5 -> (1,2,1,2,1,2,1,2,1,2)
#                           'abc' * 3 -> abcabcabc
#                           'hello' + 'world' -> hello world
#               but not 'the quick brown fox' * 10 (more than 20 characters)
# Membership Tests: Mutable are replaced by Immutable
#               when membership tests such as:
#                       if e in [1,2,3]:
#               are encountered, the [1,2,3} constant, is replaced by its immutable counterpart
#                       (1,2,3) tuple
#               list -> tuples
#               sets -> frozenset

# Set membership is much faster than list or tuple membership (set are basically like dictionaries)

# So instead of writing
#       if e in [1,2,3]: or if e in (1,2,3):
#           write if e in {1,2,3}:

# below example one of optimization of peephole


def my_func():
    a = 24 * 60  # pre calculated
    b = (1, 2) * 5  # pre calculated
    c = 'abc' * 3  # pre calculated
    d = 'ab' * 11  # more than 20 so doesn't pre calculated
    e = 'the quick brown fox' * 5  # more than 20 so doesn't pre calculated
    f = ['a', 'b'] * 3          # List is not constant so doesn't pre calculated and is mutable


print(my_func.__code__.co_consts)

# Now check the membership, list will become tuple


def my_func2(e):
    if e in [1, 2, 3]:
        pass


print(my_func2.__code__.co_consts)

# Now we will see for sets which will become frozen sets


def my_func3(e):
    if e in {1, 2, 3}:
        pass


print(my_func3.__code__.co_consts)


# set membership vs tuple or list membership
#  print(string.ascii_letters)

char_list = list(string.ascii_letters)
char_tuple = tuple(string.ascii_letters)
char_set = set(string.ascii_letters)

print(char_list)  # Order sequence
print(char_tuple)  # Order sequence
print(char_set)  # Un-order sequence

# Write a function to test the membership_test (Python is highly polymorphic


def membership_test(n, container):
    for i in range(n):
        if 'z' in container:
            pass


start = time.perf_counter()
membership_test(1000000, char_list)
end = time.perf_counter()
print('list', end-start)

start = time.perf_counter()
membership_test(1000000, char_tuple)
end = time.perf_counter()
print('list', end-start)

start = time.perf_counter()
membership_test(1000000, char_set)  # much faster than others
end = time.perf_counter()
print('list', end-start)

