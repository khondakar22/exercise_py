# The term shared reference is the concept of two variables referencing
#  the same objects in memory
# Python's emory manager decides to automatically re-use
# the memory reference
# When working with mutable objects we have to be more careful

a = "Hello"
b = a

# Now you can see the both pointing the same memory address
print(hex(id(a)))
print(hex(id(b)))

# Python does automatically for us
a = "hello"
b = "hello"

print(hex(id(a)))
print(hex(id(b)))

# Here we are not changing the value of a, so it's safe
b = "hello world"

print(hex(id(b)))

# Mutability example

a = [1, 2, 3]
b = a

# Both memory address are same, means shared references
print(hex(id(a)))
print(hex(id(b)))

b.append(100)
print(hex(id(a)))
print(hex(id(b)))
# With mutable objects, the Python memory manager
# will change the value for both
print(a)
print(b)

# don't count
a = 10
b = 10
print(hex(id(a)))
print(hex(id(b)))

# below code will change your perspective about shared reference
# Because it's a different memory address, python manager some time does the shared reference, sometimes doesn't

a = 500
b = 500
print(hex(id(a)))
print(hex(id(b)))

