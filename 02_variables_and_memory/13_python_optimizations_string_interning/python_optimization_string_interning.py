import sys
import time

#   some string are also automatically interned but not all

#   As the python code is complied, identifiers are interned
#       Variable name
#       Function names
#       class names etc..

#   Some string literals may also be automatically interned:
#       string literals that look like identifiers ( e.g 'hello_world')
#       although if it starts with a digit, even though that is not a valid identifier,
#       it may still get interned
#   Wyh do this?
#       It's all about (Speed and, possible, memory) Optimization.
#       python, both internally, and in the code write, deals with lots and lots of dictionary type
#       lookups, on string keys, which means a lot of string equality testing.
#   Let's say we want to see if two string ar equal

a = 'some_long_string'
b = 'some_long_string'

# Using
print(a == b)
# we need to compare the two strings character by character

# But if we know that 'some_long_string' has been interned, then a and b are the same string
# if they both point to the same memory address

# In which case we can use print( a is b) instead- which compares two integers (memory address)
# This is much faster than the character by character comparison

# Be careful  Not all strings are automatically interned by Python
# But you can force string to be interned by using the sys.intern() method
# first import sys

# When should you do this?
#   dealing with a large number of strings that could have high repetition
#           e.g. tokenizing a large corpus of text (NLP)
#   lots of string comparisons

# In general though, we do not need to intern strings oneself. Only do this if we really need to.


a = 'hello'
b = 'hello'

# its look like intern, because same identifier

print(id(a), id(b))
print(a == b)  # true
print(a is b)  # true

# but below are not intern

a = 'hello world'
b = 'hello world'
print(id(a), id(b))
print(a == b)  # true
print(a is b)  # false, because the memory address is different

# but for long string another example
a = '_this_is_a_long_string_that_could_be_used_as_an_identifier'
b = '_this_is_a_long_string_that_could_be_used_as_an_identifier'
print(id(a), id(b))
print(a == b)  # true
print(a is b)  # true

# But we will force to make intern
a = sys.intern('hello world')
b = sys.intern('hello world')
c = 'hello world'

print(id(a), id(b), id(c))
print(a == b)  # true
print(a is b)  # true

# one compare function to test the string intern


def compare_using_equals(n):
    x = 'a long string that is not interned' * 200
    y = 'a long string that is not interned' * 200
    for i in range(n):
        if x == y:
            pass


def compare_using_interning(n):
    x = sys.intern('a long string that is not interned' * 200)
    y = sys.intern('a long string that is not interned' * 200)
    for i in range(n):
        if x is y:
            pass


start = time.perf_counter()
compare_using_equals(10000000)
end = time.perf_counter()
print('equality', end-start)

start = time.perf_counter()
compare_using_interning(10000000)
end = time.perf_counter()
print('using (is a)', end-start)
