
a = 10
print(hex(id(a)))
print(type(a))

# now change the value of a and it will change the memory address
a = 15
print(hex(id(a)))


# same object use the same memory address, below example
a = 10
b = 10

print(hex(id(b)))
print(hex(id(a)))

