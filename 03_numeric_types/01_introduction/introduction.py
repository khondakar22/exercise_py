# Four main types of numbers:                                           Represents

# Integer Numbers (Z)  0, (+/-)1, (+/-)2, (+/-)3, ....                      int
# Rational Numbers (Q)  { p/q | p,q (E) Z, q != 0 }                         fractions.Fraction
# Real Numbers (R)      0, -1, 0.1235, 1/3, pi,....                         float, decimal.Decimal
# Complex Numbers(C)   { a + bi | a,b (E) R }                               complex

# Z subset of Q subset of R subset of C

#  Boolean truth values 0(False), 1(True)                                   bool


