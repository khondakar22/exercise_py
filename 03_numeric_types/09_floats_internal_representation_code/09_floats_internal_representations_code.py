# first look help if you need
from fractions import Fraction

help(float)

print(float(10))

print(float(10.4))

print(float('12.5'))

print(float('22/7'))  # this ValueError

a = Fraction('22/7')

print(float(a))  # Now its ok

print(0.1)

print(format(0.1, '.15f'))
#  Not exact
print(format(0.1, '.25f'))

print(0.125)

print(1/8)
# exact
print(format(0.125, '.25f'))

a = 0.1 + 0.1 + 0.1
b = 0.3

print(a == b)

print(format(a, '0.25f'))
# base two representation is not exact
print(format(b, '.25f'))
