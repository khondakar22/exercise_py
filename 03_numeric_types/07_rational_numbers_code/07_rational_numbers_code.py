from fractions import Fraction
import math
# print(help(Fraction))

# Some test

print(Fraction(1))
print(Fraction(1, 1))
print(Fraction(denominator=1, numerator=2))

print(Fraction(numerator=1, denominator=2))

print(Fraction(0.125))
print(Fraction('0.125'))
print(Fraction('22/7'))


# Arithmetic Operators

x = Fraction(2, 3)
y = Fraction(3, 4)

print(x + y)

print(x * y)

print(x / y)

print(Fraction(8, 16))

# For negative numbers

x = Fraction(1, -4)
print(x.numerator)
print(x.denominator)

# Float has finite
x = Fraction(math.pi)

print(x)

print(float(x))

y = Fraction(math.sqrt(2))

print(y)
print(float(y))

a = 0.125
print(a)

b = 0.3
print(Fraction(a))
print(Fraction(b))

# Format the b

print(format(b, '0.5f'))
print(format(b, '0.15f'))
print(format(b, '0.25f'))

x = Fraction(0.3)
print(x.limit_denominator(10))

x = Fraction(math.pi)
print(Fraction(x))

print(float(x))

print(x.limit_denominator(10))
