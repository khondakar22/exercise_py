import fractions

print(type(10))

# Internal Documentation
#
print(help(int))

print(int(True))

# For Fraction

a = fractions.Fraction(22, 7)
print(a)
print(float(a))
print(int(a)) # truncation

print(int("101", 2))
print(int("FF", 16))
print(int("ff", 16))

print(int("A", 11))
# print(int("B", 11))  // this will raise ValueError

# Built in function

print(bin(10))
print(bin(5))
print(oct(10))
print(hex(255))

# Literal
a = 0b101
print(a)


# Let's see one example

def from_base10(n, b):
    if b < 2:
        raise ValueError('Base b must be >= 2')
    if n < 0:
        raise ValueError('Base b must be >= 2')
    if n == 0:
        return [0]
    digits = []
    while n > 0:
        n, m = divmod(n, b)         # m = n % b ,  n = n // b
        digits.insert(0, m)
    return digits

# lets test


print(from_base10(25, 2))

# Need encoded for hexadecimal


def encode(digits, digit_map):
    if max(digits) >= len(digit_map):
        raise ValueError("digit_map is not log enought to encode the digits")
#    encoding = ''
#    for d in digits:
#        encoding += digit_map[d]
#    return encoding
#
#   Simpler way
#
    return ''.join([digit_map[d] for d in digits])

# let's test


print(encode([15, 15], '0123456789ABCDEF'))


# Combine both function

def rebase_from10(number, base):
    digit_map = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    if base < 2 or base > 36:
        raise ValueError('Invalid base: 2 <= base <= 36')
    sign = -1 if number < 0 else 1
    number *= sign

    digits = from_base10(number, base)
    encoding = encode(digits, digit_map)
    if sign == -1:
        encoding = '-' + encoding
    return encoding


# lets test

e = rebase_from10(10, 2)
print(e)
# back it
print(int(e, base=2))
# Other one for base= 16
e = rebase_from10(-3451, 16)
print(e)
# back it
print(int(e, base=16))
