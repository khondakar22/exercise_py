# Floats Equality testing
#
# In the previous video we saw that some decimal numbers (with a finite representation)
# cannot be represented with a finite binary representation
#
#   This can lead to some " weirdness" and bugs in our code ( but not a Python bug!!)
#
#   x = 0.1+0.1+0.1         format(x, '.25f') -> 0.3000000000000000444089210
#   y = 0.3                 format(y, '.25f') -> 0.2999999999999999888977698
#   x == y -> False
#
#   Using rounding will not necessarily solve the problem either!
#   It is no more possible to exactly represent round(0.1, 1) that 0.1 itself
#
#       round(0.1, 1) + round(0.1,1) +round(0.1,1) == round(0.3,1) -> False
#
#   But it can be used to round the entirety of both sides of the equality comparison
#
#       round(0,1+0,1+0,1, 5) == round(0.3,5) -> True
#
#   To Test for "equality" of two different floats, you could do the following methods:
#       round both sides of the equality expression to the number of significant digits
#           round(a,5) == round(b,5)
#   or, more generally, use an appropriate range(e)[epsilon} within which two number are deemed equal
#       for some e > 0,         a = b   if and only if |a-b| < e    def is_equal(x,y, eps)
#                                                                       return math.fabs(x-y) < eps
#   This can be tweaked by specifying that the difference between the two number bes a
#   percentage of their ->
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
