# An integer number is an object - an instance of the int class
# The int class provides multiple constructors

# a = int (10)
# a = int (-10)

# Other (numerical) data types are also supported in the argument of the int constructor
# a = int(10.9)             ---> truncation: a -> 10
# a = int(-10.9)            ---> truncation: a -> -10
# a = int(True)                              a -> 1
# a = int(Decimal("10.9"))  ---> truncation : a -> 10

# As well as strings (that can be parsed to a number)
# a = int("10")     a -> 10

# Number Base
# int("123)-> (123)[10]  // [] this means base
# When used with a string, constructor has an optional second parameter: base   2<=base<=36
# If base is not specified, the default is base 10-as in the example above

# int("1010",2 )        -> (10)[10]     int("1010", base=2) -> (10)[10]
# int("A12F", base=16)  -> (41263)[10]  int("534", base = 8) -> (348)10
# int("a12f" base =16)  -> (41263)[10]  int("A", base = 11) -> (10)[10]
# int("B", 11)  ValueError: invalid literal for int() with base 11: 'B'

# Reverse Process: changing an integer from base 10 to another base
# built-in functions :  bin()   bin(10) -> '0b1010'
#                       oct()   oct(10) -> '0o12'
#                       hex()   hex(10) -> '0xa'

# The prefixes in the strings help document the base of the number int('0xA', 16) -> (10)[10]
# These prefixes are consistent with literal integers using a base prefix (no strings attached!)

#   a = 0b1010  a -> 10
#   a = 0o12    a -> 10
#   a = 0xA     a -> 10

# What about other bases? Custom code
# n: number (base 10)
# b: base (target base)

#      _?_  _?_  _?_  _?_  _?_  _?_  _?_  _?_
#       b7   b6   b5   b4   b3   b2   b1   b0

#        n = b * (n//b) + n%b
#     -> n = (n//b) * b + n %b

# n = 232 b = 5
# 232   = (232 // 5) x 5 + 232 % 5 = 46 x 5 + 2
#       = [46 x 51] + [2 x 50]
#       = [((46 // 5) x 5 + 46 % 5) x 51] + [2 x 50]        _?_     _?_    _?_     _?_
#       = [(9 x 5 + 1) x 51] + [2 x 50]                     5[3]    5[2]   5[1]    5{0]
#       = [(9 x 5 + 1) x 51] + [2 x 50]
#       = [9 x 52] + [1 x 51] + [2 x 50]
#       = [((9 // 5) x 5 + 9 % 5) x 52] + [1 x 51] + [2 x 50]               46 too big
#       = [(1 x 5 + 4) x 52] + [1 x 51] + [2 x 50]            _?_     _?_    _46_     _2_
#       = [1 x 53] + [4 x 52] + [1 x 51] + [2 x 50]           5[3]    5[2]   5[1]    5[0]
#           div       3rd mod   2nd mod     1st mod
#       = [((1 // 5) x 5 + 1 % 5) x 53] + [4 x 52] + [1 x 51] + [2 x 50]
#       = [(0 x 5 + 1) x 53] + [4 x 52] + [1 x 51] + [2 x 50]
#       = [0 x 54] + [1 x 53] + [4 x 52] + [1 x 51] + [2 x 50]
#           stop      4th mod    3rd mod    2nd mod    1st mod
#                                                                            9 too big
#                                                              _?_     _9_    _1_     _2_
#                                                              5[3]    5[2]   5[1]    5[0]
#
#                                                              _1_     _4_    _1_     _2_
#                                                              5[3]    5[2]   5[1]    5[0]

# Base Change Algorithm
# n = base-10 number ( > = 0 )  b = base ( > = 2 )
#                               n = 232 , b = 5
#                               digits -> [1,4,1,2]

# if b < 2 or n < 0: raise exception
# if n ==0: return [0]

# digits = []
# while n > 0 :
#   m = n % b
#   n = n // b
#   digits.insert( 0, m )

# This algorithm returns a list of the digits in the specified base b ( a representation of n[10] in base b)
# Usually we want to return an encoded number where digits higher than 9 use letters such as A..Z
# We simply need to decide what character to use for the various digits in the base
# Encodings
# But we don't have to use letters or even standard 0-9 digits to encode our number.
# Typically, we use 0-9 and A-Z for digits required in bases higher than 10
# We just need to map between the digits in our number, to a character of our choice.

# 0 → 0         0 → 0       0 → a
# 1 → 1         1 → 1       1 → b
# …             …            …
# 9 → 9         10 → A      9 → i
# 10 → A        11 → B      10 → #
# 11 → B        …           11 → !
# …             37 → a      …
# 36 → Z        38 → b      36 → *
#                …
#                62 → z

# Python uses 0-9 and a-z (case insensitive) and is therefore limited to base <= 36
# Your choice of characters to represent the digits, is your encoding map


# The simplest way to do this given a list of digits to encode, is to create a string with as many
# characters as needed, and use their index (ordinal position) for our encoding map
# digits = [ … ]
# base b (>=2)
# map = ' … ' (of length b)
# encoding = map[digits[0]] + map[digits[1]] + …
#
# Example: Base 12
#           map = '0123456789ABC'
#           digits = [4, 11, 3, 12]
#           encoding = '4B3C'
#
#
# Encoding Algorithm
# digits = [ … ]
# map = ' … '
#
#   encoding = ''
#   for d in digits:
#       encoding += map[d] (a += b → a = a + b)
# or, more simply:
#           encoding = ''.join([map[d] for d in digits])
# we'll cover this in much more detail in the section on lists


