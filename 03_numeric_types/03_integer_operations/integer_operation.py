# Integers support all the standard arithmetic operation:   addition        +
#                                                           subtraction     -
#                                                           multiplication  *
#                                                           division        /
#                                                           exponents       **

# But what is the resulting type of each operation?
#   int + int -> int
#   int - int -> int
#   int * int -> int
#   int ** int-> int
#   int / int -> float          obviously 3/4 -> 0,75 (float)
#                                but also 10/2 -> 5 (float)


# Two more operators in integer arithmetic
# First we revisit long integer division                155      numerator
#                                                       ---     -------------
#                                                        4       denominator

#       38 -> 155 // 4
#       ----
#  4  | 155
#       12                  15 / 4 = 38 with remainder 3
#       ----
#       35                  put another way:
#       32                      155 = 4 * 38 + 3        155 = 4 * (155 // 4) + (155 % 4 )
#       ----                                                = 4 *      38    +      3
#        3 -> 155 % 4
#
#       (//) is called floor division       (%) is called the modulo operator (mod)
#       and they always satisfy: n = d * ( n // d )+ ( n % d ) , where n = numerator and d = denominator


# What is floor division exactly?
#   First define the floor of a (real) number
#       The floor of a real number a is the largest (in the standard number order) integer <= a

#   floor(3,14) -> 3
#   floor(1,9999) -> 1
#   floor(2) -> 2

# But watch out for negative numbers!
#
# floor(-3,1) -> -4
#                       -4              -3
#                     ---o-----------o---o----
#                                  -3,1
#
#   So, floor is not quite the same as truncation! a//b = floor(a/b)

# Below example
#   a = b * ( a // b) + a % b
#   a = 135
#   b = 4       135 / 4 = 33,75(33--3/4)

#   135 // 4 -> 33
#   135 % 4 -> 3
# And, in fact    a = b * ( a // b) + a % b
#                   = 4 * ( 135 // 4 ) + ( 135 % 4 )        [ Where a= 135 and b = 4]
#                   = 4 * 33 +3
#                   = 132 +3
#                   = 135 == a

# Negative Numbers
#   Be careful, a // b, is not the integer portion of a / b, it is the floor of a / b
#   For a > 0 and b > 0, these are indeed the same thing
#   But beware when dealing with negative numbers!
#
#   a = -135
#   b = 4       -135 / 4 = -33,75(-33--3/4)

#   -135 // 4 -> -34  where for the positive  135 // 4 -> 33
#   -135 % 4 -> 1                             135 % 4 -> 3
#
# And, in fact    a = b * ( a // b) + a % b
#                   = 4 * ( -135 // 4 ) + ( -135 % 4 )        [ Where a= -135 and b = 4]
#                   = (4 * -33) + 1
#                   = -136 + 1
#                   = -135 == a

#   Expanding this further....
#   a = 13 b = 4    |   a = -13 b = 4   |    a = 13 b = -4  |    a = -13 b = -4
#   13 / 4 -> 3.25  |  -13 / 4 ->-3.25  |   13 /-4 ->-3.25  |   -13 / -4 -> 3.25
#   13 // 4 -> 3    |  -13 // 4 -> -4   |   13 //-4 ->-4    |   -13 // -4 -> 3
#   13 % 4 -> 1     |  -13 % 4 -> 3     |   13 % -4 -> -3   |   -13 % -4 -> -1
#
#               In Each of these cases : a = b * ( a // b) + a % b fulfilled this equation

# Below code will help you to illustrate above integer operations 
import math

print(type(1 + 1))
print(type(2 * 3))
print(type(4 - 10))
print(type(3 ** 6))
print(type(10 / 2))

print(math.floor(3.15))
print(math.floor(3.99999))
print(math.floor(-3.14))
print(math.floor(-3.000001))
print(math.floor(-3.0000000000001)) # 4
print(math.floor(-3.0000000000000001))  # 3

# Example

a = 33
b = 16
print(a / b)
print(a // b)
print(math.floor(a / b))

# for negative number
a = -33
b = 16
print(a / b)
print(a // b)
print(math.floor(a / b))

# truncation
a = 33
b = 16
print(a / b)
print(a // b)
print(math.floor(a / b))
print(math.trunc(a / b))

# Equation a = b*(a//b)+(a%b)

a = 13
b = 4
print('{0}/{1} = {2}'.format(a, b, a / b))
print('{0}//{1} = {2}'.format(a, b, a // b))
print('{0}%{1} = {2}'.format(a, b, a % b))
print(a == b * (a // b) + (a % b))

# For negative
a = -13
b = 4
print('{0}/{1} = {2}'.format(a, b, a / b))
print('{0}//{1} = {2}'.format(a, b, a // b))
print('{0}%{1} = {2}'.format(a, b, a % b))
print(a == b * (a // b) + (a % b))

# Again negative
a = 13
b = -4
print('{0}/{1} = {2}'.format(a, b, a / b))
print('{0}//{1} = {2}'.format(a, b, a // b))
print('{0}%{1} = {2}'.format(a, b, a % b))
print(a == b * (a // b) + (a % b))

# Both Negative
a = -13
b = -4
print('{0}/{1} = {2}'.format(a, b, a / b))
print('{0}//{1} = {2}'.format(a, b, a // b))
print('{0}%{1} = {2}'.format(a, b, a % b))
print(a == b * (a // b) + (a % b))
