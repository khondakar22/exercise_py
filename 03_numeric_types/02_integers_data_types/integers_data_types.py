# The int data types (Its a object)
# Ex: 0, 10, 100, 1000000....
# How large can a python int become (positive or negative) ?
# Integers are represented internally using base-2(binary) digits, not decimal.

#  (individual bits[0|1])       1       0       0       1       1
#       ___     ___     ___     ___     ___     ___     ___     ___
#       2^7     2^6     2^5     2^4     2^3     2^2     2^1     2^0
#       128     64      32      16      8       4       2       1

# Calculation

#       1 x 16 + 0 x 8 + 0 x  + 1 x 2 + 1 x 1 = 16 + 2 + 1 = 19
#       (10011)[base2]  = (19)[base10]

# Representing the decimal number 19 requires 5 bits


# What's the largest (base 10) integer number that can be represented using 8bits?
# Lets assume first that we only care about non-negative integers

#       1       1       1       1       1       1       1       1
#       ___     ___     ___     ___     ___     ___     ___     ___
#       2^7     2^6     2^5     2^4     2^3     2^2     2^1     2^0
#       128     64      32      16      8       4       2       1

#       128 + 64 + 32 + 16 + 8 + 4 + 2 + 1 = 255 = 2^8 -1

# If we care about handling negative integers as well, then 1 bit is reserved to represent the sign
# of the number, leaving us with only 7 bits for the number itself out of the original 8 bits

# The largest number we can represent using 7 bits is 2^7 -1 = 127
# So, using 8 bits we are able to represent all the integers in the range [-127, 127]

# Since 0 does not require a sign, we can squeeze out an extra number, and
# We end up with the range [-128, 127] formula is [-2^7, 2^7-1]

# If we want to use 16 bits to store (signed) integers, our range would be:
#   2^(16-1) = 2^15 = 32,768 Range: [-32,768 ... 32,767 ]
# Similarly, if we want to use 32 bits to store (signed) integers, our range would be:
#   2^(32-1) = 2^31 = 2,147,483,648 Range: [-2,147,483,648 ... 2,147,483,647 ]
# If we had an unsigned integer type, using 32 bits our range would be:
#   [0, 2^32 ] = [0 ... 4,294,967,296 ]

#   One Example
#   -----------------------------------------------------------------------------
#   |In a 32-bit OS:                                                            |
#   |   memory space (bytes) are limited by their address number -> 32 bits     |
#   |   4,294,967,296 bytes of addressable memory                               |
#   |   = 4,294,967,296 / 1024 kB = 4,194,304 kB                                |
#   |   = 4,194,304 / 1024 MB = 4,096 MB                                        |
#   |   =   4,096 / 1024 GB = 4GB                                               |
#   -----------------------------------------------------------------------------
#

# So, how large an integer can be depends on how many bits are used to store the number.
# Some languages (such as Java, C, .. ) provide multiple distinct integer data types that
# use a fixed number of bits:
#       In Java
#           byte    signed 8-bit numbers    -128, ... , 127
#           short   signed 16-bit numbers   -32,768, ... , 32,767
#           int     signed 32-bit numbers   -2^31, ... , 2^31-1
#           long    signed 64-bit numbers   -2^63, ... , 2^63-1

# but Python does not work this way
# The int object uses a variable number of bits
# Can use 4 bytes (32 bits), 8 bytes (64 bits), 12 bytes (96 bits), etc.
# [ since int(s) are actually objects, there is a further fixed overhead per integer]
# Theoretically limited only by the amount of memory available
#   Larger numbers will use more memory and standard operators such as +,*, etc. will run slower
#   as numbers get larger

# Below example will illustrate better way..
import sys
import time
print(type(100))
print(sys.getsizeof(0))


def calc(c):
    for i in range(10000000):
        c * 2


# This will calculate faster
start = time.perf_counter()
calc(10)
end = time.perf_counter()
print(end - start)

start = time.perf_counter()
calc(2*100)
end = time.perf_counter()
print(end - start)


start = time.perf_counter()
calc(2*10000)
end = time.perf_counter()
print(end - start)


