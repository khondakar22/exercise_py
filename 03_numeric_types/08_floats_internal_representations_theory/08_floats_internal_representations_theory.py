#  The float class is Python's default implementation for
#  representing real numbers
#  The Python(CPython) float is implemented using the C double type
#  which(usually!) implements the IEE 754 double-precision binary
#  float, also called binary64

#  The Float uses a fixed number of bytes -> 8 bytes (but Python objects have some overhead too)
#                                         -> 64 bits    -> 24 bytes (CPython 3.6 64bit)

# These 64 bits are used up as follows:
#   sign        -> 1 bit
#   exponent    -> 11 bits -> range[-1022,1023]        1.5E-5 -> 1.5x10^-5
#   significant digits -> 52 bits   -> 15-17 significant(base-10) digits
#   what is significant digits -> for simplicity, all digits except leading and trailing zeros
#       1.2345  1234.5  1234500000  0.00012345   12345e-50  1.2345e10

#   Representation: Decimal
#   Numbers can be represented as base-10 integers and fractions:
#
#   0.75  ->    7         5
#              ----  +  -----    -> 7x10^-1 + 5x10^-2    2 significant digits
#               10       100
#
#   0.256 ->    2          5         6
#              ----  +   -----  +  ------  -> 2x10^-1 + 5x10^-2 + 6x10^-3    3 significant digits
#               10        100       1000
#
#   123.456->                  4       5       6
#             1x100+2x10+3x1+ ---- + ----- + ------              6 significant digits
#                              10     100     1000
#
#          -> 1x10^2 + 2x10^1 +3x10^0 + 4x10^-1 + 5x10^-2 + 6x10^-3
#
# In General (For Finite Number)
#
#           d = sum of(E)(range n to i = -m) d(i) x 10^i
#
# sign = 0 for positive
# sign = 0 for negative
#
#           d = (-1)^sign sum of(E)(range n to i = -m) d(i) x 10^i
#
#
# Some numbers cannot be represented using a finite number of terms
#
#   pi = 3.14159...
#   square root(2)=1.4142...
#
# but even some rational numbers
#
#   1
#  --- = 0.3333...
#   3
#
#      3        3       3
#     ---- +  ----- + ------ + ....
#      10      100     1000
#
# Representation: binary
#   Numbers in a computer are represented using bits, not decimal digits
#   -> instead of power of 10, we need to use powers of 2
#
#   (0.11)[2}     1         1
#           = ( -----  +  ----- )[10] = (0.5+0.25)[10]
#                 2         4
#
#           = (1x2^-1+1x2^-2)10
#
#   Similarly,
#                        1     1     0     1
#       (0.1101)[2] = ( --- + --- + --- + ---- )[10] = (0.5+0.25+0.0625)10 = (0.8125)[10]
#                        2     4     8     16
#                   = (1x2^-1+1x2^-2+0x2^-3+1x2^-4)10
#
# This representation is very similar to the one we use with decimal number but
#   instead of using powers of 10, we use powers of 2
#   a binary representation
#            d = (-1)^sign sum of(E)(range n to i = -m) d(i) x 2^i
#
#
# The same problem that occurs when trying to represent 1/3 using a decimal expansion also
# happens when trying to represent certain numbers using a binary expansion
#
# 0.1 = 1/10 Using binary fractions, this number does not have a finite representation
#
#   (0.1)[10] = (0.0001100110011..)[2]  // [] means powers of number, if 2 then binary, 10 then digits
#
#
# So, some numbers that do have a finite decimal representation,
#   do not have a finite binary representation
#   and some do
#
#   (0.75)[10] = (0.11)[2]      finite
#                                       -> exact float representation
#   (.8125)[10] = (0.1101)[2]   finite
#
#   (0.1)[10] = (0001100110011..)[2] infinite -> approximate float representation
#
